<!DOCTYPE html>
<html ng-app="">
  <head>
    <title>Laemtong Contact Information</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <link href="assets/css/bootstrap.min.css" media="all" rel="stylesheet">
    <link href="assets/typeface/typeface.css" media="all" rel="stylesheet">
    <link href="assets/css/load.css" media="all" rel="stylesheet">
  </head>
  <body>
    <div class="overlay"></div>
    <nav role="navigation">
      <h1 class="branding clearfix">
        <a href="http://www.laemthong.com/">
          <img height="16" src="assets/img/laemtong-logo@2x.png" width="209">
        </a>
      </h1>
    </nav>
    <section ng-controller="AddressCtrl" role="container">
      <ul class="list-unstyled">
        <li class="clearfix" ng-repeat="contact in contacts">
          <h2 class="logo">
            <img alt="{{contact.titleEN}}" class="at2x" height="{{contact.imageHeight}}" src="{{contact.image}}" width="{{contact.imageWidth}}">
          </h2>
          <p>
            {{contact.titleTH}}
            <br>
            {{contact.address}}
          </p>
          <div class="meta">
            <span class="phone">
              <a href="tel:{{contact.phone}}">T {{contact.phone}}</a>
            </span>
            <span class="fax">
              <a href="tel:{{contact.fax}}">F {{contact.fax}}</a>
            </span>
          </div>
          <br>
          <div class="action">
            <a class="btn btn-savetophone" href="tel:+66819827711">Save to Phone</a>
            <a class="btn btn-makeacall" href="tel:+66819827711">Call now</a>
          </div>
        </li>
      </ul>
    </section>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.9/angular.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/address.js"></script>
    <script src="assets/js/load.js"></script>
  </body>
</html>
